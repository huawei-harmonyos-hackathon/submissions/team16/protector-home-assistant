# Protector-Home-Assistant
## Hardware
1. Raspberry pi 3b+
2. Wemos temperature humidity sensor
Detailed info here: https://gitlab.com/harmonyos-hackathon/submissions/team16/esp8266-gas-temp-humidity-sensor-mqtt
3. Shelly 2.5
Shelly 2.5 is 2 relay module with power measurement.
4. Bulb socket with cables https://www.tme.eu/pl/details/pw-d.3006ma/zrodla-swiatla-oprawy/pawbol/d-3006ma/
5. Bulb https://www.amazon.com/Philips-455717-Equivalent-Daylight-Light/dp/B00XD8GKAA/?th=1
6. Extension cord https://botland.com.pl/listwy-zasilajace-i-przedluzacze/7169-listwa-zasilajaca-esperanza-elk101k-rainbow-5-pro-15m-czarna-5901299935026.html
7. 2 x WAGO 3 way connector https://www.amazon.com/Wago-221-413-LEVER-NUTS-Conductor-Connectors/dp/B017NQWDY4/ref=sr_1_6?keywords=wago&qid=1638112783&sr=8-6&th=1 

### Hardware connections
[Hardware Connections](Hardware Connectons.jpg)

## Setup
### HAOS install
Install HAOS using:
https://www.home-assistant.io/installation/raspberrypi/

### Addons
#### HAOS SSH
HAOS SSH is not needed but good to have addon:
https://smarthomepursuits.com/connect-to-home-assistant-ssh/

#### Mosquitto
For connecting Wemos temperature humidity sensor you need MQTT broker.
https://github.com/home-assistant/addons/blob/master/mosquitto/DOCS.md

## Advanced configuration - needed later
https://www.home-assistant.io/getting-started/configuration/

## Using YAML configurations
### Add wemos temperature humidity sensor to configuration
Using file editor change configuration yaml.
[General configuration](configuration.yaml)

### Add power automation
In configuration->automations add new automation. Use config below:
[Power Automation config](power-automation.yaml)
If you want to change power trigger level in line 9("above") put new power in Watts.
If you want to change time after which automation is triggered in lines 11-14 put corresponding time.

### Add temperature automation
In configuration->automations add new automation. Use config below:
[Temperature Automation config](temperature-automation.yaml)
If you want to change temperature trigger level in line 6 change ("above") settings to new temperature.
If you want to change time after which automation is triggered in 8-11 put corresponding time.
### Add new dashboard
In configuration->dashboard add new dashboard. Use config below:
[Dashboard config](dashboard-config.yaml)

## How it works
### Starting charger
In charging dashboard click "Start charging". Shelly Relay output 1 goes on.
### Scenario 1
If power go more than 20Watts - or changed value, the Relay output 1 will go off, shelly output 2 will go on alarming the charging problem.
### Scenario 2
If temperature go more than 30 celcius, the Relay output 1 will go off, shelly output 2 will go on alarming the charging problem.
### Dashboard features
Dashboard shows:
1. Current
2. Temperature
3. Alarm state
